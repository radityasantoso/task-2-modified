const binarySearch = (value, list) => {
    let low = 0
    let high = value.length - 1
  
    while (low <= high) {
      const mid = Math.floor((low + high) / 2)
      const guess = value[mid]
  
      if (guess === list) {
        return mid
      }
  
      if (guess > list) {
        high = mid - 1
      } else {
        low = mid + 1
      }
    }
  
    return null 
  }
console.log(binarySearch([1,2,3,4,5,], 3));
